<?php

namespace app\core;

use Yii;
use yii\web\UrlManager;

class CoreUrlManager extends UrlManager
{
    

    public function createUrl($params)
    {
        $url = parent::createUrl($params);
        $langSlug = Yii::$app->request->get('langID', 'ro');

        if ($url == '/')
        {
            return '/' . $langSlug . '/';
        } 
        else
        {
            return '/' . $langSlug . $url;
        }
    }

}
