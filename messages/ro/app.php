<?php

return [
    'General settings' => 'Setari generale',
    'Default language' => 'Limba implicita',
    
    'SunatiLa' => 'Sunați la',
    'Contacts' => 'Contacte',
    'Vezi produsul' => 'Vezi produsul',
    'Produsele noastre' => 'Produsele noastre',
    'Detalii' => 'Detalii',
    'Serviciile noastre' => 'Serviciile noastre',
    'Comanda acum!' => 'Comanda acum!',
    'Despre noi!' => 'Despre noi!',
    'Vezi Articol' => 'Vezi Articol',
];