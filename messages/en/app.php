<?php

return [
    'General settings' => 'General settings',
    'Default language' => 'Default language',
    
    'SunatiLa' => 'Phone to',
    'Contacts' => 'Contacts',
    'Vezi produsul' => 'View product',
    'Produsele noastre' => 'Our Products',
    'Detalii' => 'Details',
    'Serviciile noastre' => 'Serviciile noastre',
    'Comanda acum!' => 'Comanda acum!',
    'Despre noi!' => 'Despre noi!',
    'Vezi Articol' => 'View Articole',
];