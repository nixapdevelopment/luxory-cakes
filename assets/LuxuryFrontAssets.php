<?php

namespace app\assets;

use yii\web\AssetBundle;


class LuxuryFrontAssets extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/luxury/assets';
    
    public $css = [
        'https://fonts.googleapis.com/css?family=Satisfy',
        'css/animate.css',
        'js/owl/assets/owl.carousel.min.css',
        'css/uikit.almost-flat.min.css',
        'css/uikit.min.css',        
        'js/owl/assets/owl.theme.default.min.css',
        'css/main.css',
    ];
    
    public $js = [
        'js/jquery.js',
        'js/uikit.min.js',
        'js/owl/owl.carousel.min.js',
        'js/scripts.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
    
}