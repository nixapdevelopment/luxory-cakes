<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Location\Location */

$this->title = 'Update Location: ' . reset($langsModels)->Title;
$this->params['breadcrumbs'][] = ['label' => 'Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => reset($langsModels)->Title];
?>
<div class="location-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'langsModels' => $langsModels,
        'imagesModels' => $imagesModels,
        'locationCategoryModels' => $locationCategoryModels,
        'categoryModels' => $categoryModels
    ]) ?>

</div>
