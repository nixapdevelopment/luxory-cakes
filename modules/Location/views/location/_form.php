<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Location\Location */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="location-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>
    
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Main',
                'content' => $this->render('_main_form', [
                    'model' => $model,
                    'langsModels' => $langsModels,
                    'form' => $form,
                    'locationCategoryModels' => $locationCategoryModels,
                    'categoryModels' => $categoryModels
                ])
            ],
            [
                'label' => 'Images',
                'content' => $this->render('_image_form', [
                    'imagesModels' => $imagesModels,
                    'form' => $form,
                    'model' => $model
                ])
            ],
            [
                'label' => '<span id="location-picker-hack">Location on map</span>',
                'encode'=>false,
                'content' => $this->render('_map_form', [
                    'form' => $form,
                    'model' => $model,
                ])
            ],
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    <?php $this->registerJs(
        "$('#location-picker-hack').closest('a[data-toggle=\"tab\"]').on('click', function() { setTimeout(function(){ $('.location-picker').locationpicker('autosize'); }, 200) });"
    ); ?>

</div>
