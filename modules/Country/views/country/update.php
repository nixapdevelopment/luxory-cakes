<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Country\Country */

$this->title = 'Update Country: ' . reset($model->countryLangs)->Name;
$this->params['breadcrumbs'][] = ['label' => 'Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => reset($model->countryLangs)->Name, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="country-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'langsModels' => $langsModels,
        'countryRegionsDataProvider' => $countryRegionsDataProvider
    ]) ?>

</div>
