<?php

    use yii\bootstrap\NavBar;
    use yii\bootstrap\Nav;
    
    $mainItems = [];
    $quickAddItems = [];
    foreach ($articleTypes as $type => $articleType)
    {
        $mainItems[] = [
            'label' => $articleType['labels']['plural'], 
            'url' => ['/admin/article', 'type' => $type]
        ];
        $quickAddItems[] = [
            'label' => '<i style="width:20px;text-align: center" class="' . $articleType['labels']['icon'] . '" aria-hidden="true"></i> ' . $articleType['labels']['add'], 
            'url' => [
                '/admin/article/article/create', 
                'type' => $type,
                'hash' => sha1(microtime())
            ], 
            'encode' => false
        ];
    }
    
    $mainItemsGroup[] = [
        'label' => 'Content',
        'items' => $mainItems
    ];

    NavBar::begin([
        'brandLabel' => 'Nixap CMS',
        'brandUrl' => yii\helpers\Url::to('/admin'),
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    
    if (Yii::$app->user->isGuest)
    {
        $menuItems = [
            ['label' => 'Login', 'url' => ['/admin/user/login']]
        ];
    }
    else
    {
        $leftItems = [
            ['label' => 'Dashboard', 'url' => ['/admin/dashboard']],
            [
                'label' => 'Quick add',
                'items' => $quickAddItems,
            ]
        ];
        $rightItems = [
            ['label' => 'Menu', 'url' => ['/admin/menu']],
            ['label' => 'Slider', 'url' => ['/admin/slider']],
            ['label' => 'Feedback', 'url' => ['/admin/feedback']],
            ['label' => 'Settings', 'url' => ['/admin/settings']],
            ['label' => 'Logout', 'url' => ['/admin/user/login/logout']],
        ];
        
        $menuItems = yii\helpers\ArrayHelper::merge($leftItems, $mainItemsGroup, $rightItems);
    }
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems
    ]);
    NavBar::end();
?>