<?php

namespace app\modules\Article\components\ArticleFileWidget;

use yii\base\Widget;


class ArticleFileWidget extends Widget
{
    
    public $article = null;
    
    public $query = null;
    
    public $view = 'files';


    public function init()
    {
        if ($this->article == null)
        {
            throw new InvalidParamException('ArticleImageWidget::$article is required');
        }
        
        parent::init();
    }

    public function run()
    {
        return $this->render($this->view, [
            'article' => $this->article
        ]);
    }
    
}