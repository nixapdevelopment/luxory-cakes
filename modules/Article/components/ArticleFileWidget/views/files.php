<?php

    use yii\helpers\Html;
    use bl\files\icons\FileIconWidget;
    
    $iconWidget = FileIconWidget::begin([
        'useDefaultIcons' => true
    ]);

?>

<div>
    <?php foreach ($article->files as $file) { ?>
    <?= Html::a(
        $iconWidget->getIcon($file->File) . ' ' . $file->File,
        $file->fullLink, 
        [
            'title' => $file->File,
            'class' => 'alert alert-info',
            'style' => 'display:block;',
            'target' => '_blank'
        ]) 
    ?>
    <?php } ?>
</div>