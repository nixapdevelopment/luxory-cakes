<?php

namespace app\modules\Article;

use app\core\CoreModule;

/**
 * Article module definition class
 */
class Article extends CoreModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Article\controllers';
    
    public $name = 'Article';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    
    public function articleTypes()
    {
        return [
            'Page' => [
                'labels' => [
                    'icon' => 'fa fa-file-text-o', // fa or glyphicon full icon name
                    'singular' => 'Page',
                    'plural' => 'Pages',
                    'add' => 'Add page',
                    'wasSaved' => 'Page was saved',
                ],
                'hierarchy' => false,
                'hasChronology' => false,
                'gridViewColumns' => [
                    'lang.Title',
                    [
                        'label'  => 'Text',
                        'value' => function($article) {
                            return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
                        }
                    ],
                    'Status'
                ],
                'hasImages' => true,
                'hasFiles' => true,
            ],
            'Blog' => [
                'labels' => [
                    'icon' => 'fa fa-newspaper-o', // fa or glyphicon full icon name
                    'singular' => 'Blog article',
                    'plural' => 'Blogs',
                    'add' => 'Add blog article',
                    'wasSaved' => 'Blog was saved',
                ],
                'hierarchy' => false,
                'hasChronology' => true,
                'gridViewColumns' => [
                    'lang.Title',
                    [
                        'label'  => 'Text',
                        'value' => function($article) {
                            return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
                        }
                    ],
                    [
                        'label'  => 'Date',
                        'value' => function($article) {
                            return date('d.m.Y', strtotime($article->Date));
                        }
                    ],
                    [
                        'attribute' => 'Status',
                    ]
                ],
                'hasImages' => true,
                'hasFiles' => true,
            ],
            'Services' => [
                'labels' => [
                    'icon' => 'fa fa-bullhorn',
                    'singular' => 'Service',
                    'plural' => 'Services',
                    'add' => 'Add new service',
                    'wasSaved' => 'Service saved',
                ],
                'hierarchy' => false,
                'hasChronology' => true,
                'gridViewColumns' => [
                    'lang.Title',
                    [
                        'label'  => 'Text',
                        'value' => function($article) {
                            return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
                        }
                    ],
                    [
                        'label'  => 'Date',
                        'value' => function($article) {
                            return date('d.m.Y', strtotime($article->Date));
                        }
                    ],
                    'Status'
                ],
                'hasImages' => true,
                'hasFiles' => true,
            ],
            'Product' => [
                'labels' => [
                    'icon' => 'fa fa-bullhorn',
                    'singular' => 'Product',
                    'plural' => 'Products',
                    'add' => 'Add new product',
                    'wasSaved' => 'Product saved',
                ],
                'hierarchy' => false,
                'hasChronology' => true,
                'gridViewColumns' => [
                    'lang.Title',
                    [
                        'label'  => 'Text',
                        'value' => function($article) {
                            return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
                        }
                    ],
                    [
                        'label'  => 'Date',
                        'value' => function($article) {
                            return date('d.m.Y', strtotime($article->Date));
                        }
                    ],
                    'Status'
                ],
                'hasImages' => true,
                'hasFiles' => true,
            ],
        ];
    }
    
    public function getArticleConfig($articleType)
    {
        $types = $this->articleTypes();
        
        if (!isset($types[$articleType]))
        {
            throw new \yii\base\InvalidConfigException("Unknown article type. Please verify configuration in " . __CLASS__ . "::articleTypes().");
        }
        
        return $types[$articleType];
    }
    
}
