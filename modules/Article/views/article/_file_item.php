<?php

    use yii\bootstrap\Html;

?>

<li class="list-group-item">
    <div class="row">
        <div class="col-md-6">
            <?= Html::a($model->File, '@web/uploads/article/' . $model->File, [
                'target' => '_blank',
                'data-pjax' => 0
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $model->Type ?>
        </div>
        <div class="col-md-3">
            <?= date('d.m.Y - H:i', strtotime($model->Date)) ?>
        </div>
        <div class="col-md-1 text-center">
            <?= Html::a('<i class="fa fa-trash text-danger"></i>', ['delete-file', 'id' => $model->ID], [
                'data-pjax' => false,
                'data-delete-file' => true
            ]) ?>
        </div>
    </div>
</li>

<?php $this->registerJs('
    $("a[data-delete-file]").on("click", function(e){
        e.preventDefault();
        var url = $(this).attr("href");
        $.get(url, {}, function() {
            $.pjax.reload({container: "#files-list-pjax"});
        });
        return false;
    });
') ?>