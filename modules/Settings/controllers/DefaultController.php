<?php

namespace app\modules\Settings\controllers;

use Yii;
use app\controllers\BackendController;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use kartik\checkbox\CheckboxX;
use app\models\Settings\Settings;
use app\components\UploadedFile;
use kartik\widgets\FileInput;
use yii\caching\TagDependency;

/**
 * Default controller for the `Settings` module
 */
class DefaultController extends BackendController
{
    
    const TYPE_SELECT = 'select';
    const TYPE_INTIGER = 'intiger';
    const TYPE_IMAGE = 'image';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_VARCHAR = 'varchar';
    const TYPE_TEXT = 'text';
    const TYPE_LANG_TEXT = 'langText';
    
    
    public $settingsData = [];


    public function settingsList()
    {
        return [
            'General' => [
                'label' => Yii::t('app', 'General settings'),
                'items' => [
                    'defaultLanguage' => [
                        'type' => self::TYPE_SELECT,
                        'label' => Yii::t('app', 'Default language'),
                        'dropdownOptions' => function(){
                            $options = [];
                            foreach (Yii::$app->params['siteLanguages'] as $lang)
                            {
                                $options[$lang] = $lang;
                            }
                            return $options;
                        },
                    ],
                    'articlesPerPage' => [
                        'type' => self::TYPE_INTIGER,
                        'label' => 'Articles per page',
                    ],                                       
                    'logoText' => [
                        'type' => self::TYPE_VARCHAR,
                        'label' => 'Logo Text',
                    ],                    
                    'favicon' => [
                        'type' => self::TYPE_IMAGE,
                        'label' => 'Favicon',
                    ], 
                ],
                'active' => true
            ],
            'Contacts' => [
                'label' => Yii::t('app', 'Contacts'),
                'items' => [
                    'phone' => [
                        'type' => self::TYPE_VARCHAR,
                        'label' => 'Phone',
                    ],
                    'email' => [
                        'type' => self::TYPE_VARCHAR,
                        'label' => 'Email',
                    ],
                    'address' => [
                        'type' => self::TYPE_LANG_TEXT,
                        'label' => 'Address',
                    ],
                    'map' => [
                        'type' => self::TYPE_VARCHAR,
                        'label' => 'Map',
                    ],
                ]
            ],
            'Social' => [
                'label' => Yii::t('app', 'Social'),
                'items' => [
                    'facebook' => [
                        'type' => self::TYPE_VARCHAR,
                        'label' => 'Facebook Link',
                    ],
                    'instagram' => [
                        'type' => self::TYPE_VARCHAR,
                        'label' => 'Instagram Link',
                    ],
                    'youtube' => [
                        'type' => self::TYPE_VARCHAR,
                        'label' => 'Youtube Link',
                    ],
                    'twitter' => [
                        'type' => self::TYPE_VARCHAR,
                        'label' => 'Twitter Link',
                    ],
                ]
            ],
        ];
    }
    
    public function init()
    {
        parent::init();
        
        $settingsModels = Settings::find()->indexBy('Name')->all();
        foreach ($settingsModels as $settingName => $settingModel)
        {
            $value = @unserialize($settingModel->Value);
            if ($value === FALSE)
            {
                $value = $settingModel->Value;
            }
            $this->settingsData[$settingName] = $value;
        }
    }


    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionSettings()
    {
        if (Yii::$app->request->isPost)
        {
            $settingsData = (array)Yii::$app->request->post('Settings');
            
            // save text settings
            foreach ($settingsData as $settingName => $settingValue)
            {
                $model = $this->getSettingModel($settingName);
                $model->Value = is_array($settingValue) ? serialize($settingValue) : $settingValue;
                $model->save();
            }
            
            // save settings files
            $settingsFiles = UploadedFile::getInstancesByName('Settings');
            foreach ($settingsFiles as $settingName => $settingFile)
            {
                $fileName = md5($settingFile->name . microtime(true)) . '.' . $settingFile->extension;
                if ($settingFile->saveAs(Yii::getAlias('@webroot/uploads/settings/' . $fileName)))
                {
                    $model = $this->getSettingModel($settingName);
                    $model->Value = $fileName;
                    $model->save();
                }
            }
            
            TagDependency::invalidate(Yii::$app->cache, ['Settings']);
            
            Yii::$app->session->setFlash('success', 'Settings updated');
            
            return $this->redirect(['/admin/settings']);
        }
        
        $settings = $this->settingsList();
        
        $settingsTabs = [];
        foreach ($settings as $groupName => $group)
        {
            $content = '<br />';
            foreach ($group['items'] as $settingName => $settingsOption)
            {
                if (is_array($settingsOption))
                {
                    $content .= '<div class="form-group">';
                    $content .= Html::label($settingsOption['label']);
                    $content .= $this->getSettingOptionControl($settingName, $settingsOption);
                    $content .= '</div>';
                }
                else
                {
                    $content .= $settingsOption;
                }
                
            }
            
            $settingsTabs[] = [
                'label' => $group['label'],
                'content' => $content,
                'active' => !empty($group['active'])
            ];
        }
        
        return $this->render('settings-form', [
            'settingsTabs' => $settingsTabs
        ]);
    }
    
    private function getSettingOptionControl($settingName, $settingsOption)
    {
        $value = $this->settingsData[$settingName];
        
        switch ($settingsOption['type'])
        {
            case self::TYPE_SELECT:
                return Html::dropDownList("Settings[$settingName]", $value, $settingsOption['dropdownOptions'](), [
                    'class' => 'form-control'
                ]);
            case self::TYPE_INTIGER:
                return Html::input('number', "Settings[$settingName]", $value, [
                    'class' => 'form-control'
                ]);
            case self::TYPE_IMAGE:
                return FileInput::widget([
                    'name' => "Settings[$settingName]",
                    'pluginOptions' => [
                        'initialPreview'=>[
                            Yii::getAlias("@web/uploads/settings/$value"),
                        ],
                        'initialPreviewAsData' => true,
                        'initialCaption'=>"The Moon and the Earth",
                        'initialPreviewConfig' => [
                            
                        ],
                        'overwriteInitial' => true,
                        'maxFileSize' => 2000,
                        'showCaption' => false,
                        'showRemove' => false,
                        'showUpload' => false,
                    ]
                ]);
            case self::TYPE_CHECKBOX:
                return CheckboxX::widget([
                    'name' => "Settings[$settingName]",
                    'pluginOptions' => [
                        'threeState' => false
                    ]
                ]);
            case self::TYPE_VARCHAR:
                return Html::input('text', "Settings[$settingName]", $value, [
                    'class' => 'form-control'
                ]);
            case self::TYPE_TEXT:
                return Html::textarea("Settings[$settingName]", $value, [
                    'class' => 'form-control'
                ]);
            case self::TYPE_LANG_TEXT:
                $settingLangTabs = [];
                foreach (\Yii::$app->params['siteLanguages'] as $key => $lang)
                {
                    $settingLangTabs[] = [
                        'label' => strtoupper($lang),
                        'content' => Html::textarea("Settings[$settingName][$lang]", $value[$lang], [
                            'class' => 'form-control'
                        ]),
                        'active' => $key == 0
                    ];
                }
                
                return Tabs::widget([
                    'items' => $settingLangTabs
                ]);
        }
    }
    
    private function getSettingModel($settingName)
    {
        $model = Settings::findOne(['Name' => $settingName]);
        
        if (empty($model->ID))
        {
            return new \app\models\Settings\Settings([
                'Name' => $settingName
            ]);
        }
        
        return $model;
    }
    
}