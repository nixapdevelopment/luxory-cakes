<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\News\News */

$this->title = 'Create Article';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pageLangModels' => $pageLangModels,
    ]) ?>

</div>
