<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\SorocaFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;

$bundle = SorocaFrontAssets::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="favico.ico" type="image/ico">
    <title><?= Html::encode($this->title) ?></title>
    <script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>';</script>
    <?php $this->head() ?>
</head>
<body class="soroca-front">
<?php $this->beginBody() ?>
    
<header>
    <div class="container">
        <div class="top-bar">
            <div class="pull-left text-muted">
                Site-ul oficial Soroca
            </div>
            <div class="pull-right">
                <ul>
                    <li class="text-danger">
                        Feedback
                    </li>
                    <li class="text-danger">
                        <a href="<?= Url::home() ?>">
                            <?= Html::img($bundle->baseUrl . '/images/ro.jpg') ?>
                        </a>
                    </li>
                    <li>
                        <i class="fa fa-sign-in" aria-hidden="true"></i> Logare
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="header-bar">
            <div class="row">
                <div class="col-md-3">
                    <a href="/">
                        <?= Html::img($bundle->baseUrl . '/images/logo.jpg') ?>
                    </a>
                </div>
                <div class="col-md-9">
                    <?= MenuWidget::widget([
                        'menuId' => 3,
                        'options' => [
                            'class' => 'nav navbar-nav top-menu'
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</header>

<?= $content ?>
    
<br />
<br />
<br />
    
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <table>
                    <tr>
                        <td>
                            <a href="/">
                                <?= Html::img($bundle->baseUrl . '/images/logo-small.png') ?>
                            </a>
                        </td>
                        <td style="padding-left: 30px;">
                            Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-5">
                <div class="social-wrap text-center">
                    <a href="/">
                        <?= Html::img($bundle->baseUrl . '/images/social/fb.png') ?>
                        <img src="" />
                    </a>
                    <a href="/">
                        <?= Html::img($bundle->baseUrl . '/images/social/tw.png') ?>
                    </a>
                    <a href="/">
                        <?= Html::img($bundle->baseUrl . '/images/social/ok.png') ?>
                    </a>
                    <a href="/">
                        <?= Html::img($bundle->baseUrl . '/images/social/in.png') ?>
                    </a>
                    <a href="/">
                        <?= Html::img($bundle->baseUrl . '/images/social/g+.png') ?>
                    </a>
                    <a href="/">
                        <?= Html::img($bundle->baseUrl . '/images/social/vk.png') ?>
                    </a>
                </div>
            </div>
            <div class="col-md-2 text-center">
                Copyright &COPY; 2017<br>
                Creat de Nixap
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
