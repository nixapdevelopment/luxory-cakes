<?php

    use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    use app\modules\Settings\Settings;
    
?>

<div class="container">
    <h1><?= $article->lang->Title ?></h1>
    <div>
        <?= $article->lang->Text ?>
    </div>
    <div>
        <?= Settings::getByName('address', true) ?>
    </div><br />
    
    <?= ArticleImageWidget::widget([
        'article' => $article
    ]) ?>
    
    <?= ArticleFileWidget::widget([
        'article' => $article
    ]) ?>
    
    <?= FeedbackWidget::widget() ?>
    
</div>