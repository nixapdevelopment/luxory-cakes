<?php

    use yii\helpers\Html;
    use app\assets\LuxuryFrontAssets;
    use app\models\SliderItem\SliderItem;
    use app\models\Article\Article;
    use yii\caching\TagDependency;
    
    $bundle = LuxuryFrontAssets::register($this);
    
    $services = Article::getDb()->cache(function ($db) {
        return Article::find()->with(['lang', 'link'])->where(['Type' => 'Services'])->orderBy('Date DESC')->limit(3)->all();
    }, 30);
            
    $slides = SliderItem::getDb()->cache(function ($db) {
        return SliderItem::find()->with('lang')->where(['SliderID' => 1])->all();
    }, 30, new TagDependency(['tags' => 'slider']));
    
    $products = Article::getDb()->cache(function ($db) {
        return Article::find()->with(['lang', 'link'])->where(['Type' => 'Product'])->orderBy('Date DESC')->limit(6)->all();
    }, 30);
    
    $about = Article::getDb()->cache(function ($db) {
        return Article::find()->with(['lang'])->where(['ID' => 36])->limit(1)->one();
    }, 30);
     
    
    $this->registerJs("
        $(document).ready(function(){
            $('#main-slider').owlCarousel({
                loop: true,
                dots: false,
                margin: 30,
                nav: true,
                autoplay: true,
                autoplayTimeout: 4000,
                navText: [ '" . Html::img($bundle->baseUrl . '/images/prev.png' ) . "','" .  Html::img($bundle->baseUrl . '/images/next.png' ) . "' ],
                responsive: {
                    0: {
                        items:1
                    }
                }
            });
        });
    ", yii\web\View::POS_READY);

?>


<div class="uk-clearfix slider">
    
    <?php if (count($slides) > 0) { ?>

    <div id="main-slider" class="owl-carousel owl-theme">

    <?php foreach ($slides as $slide) { ?>
        <div class="item">
            <?= Html::img($slide->imageUrl) ?>
            <div class="caption">
                <div class="table">
                    <div class="middle">
                        <div class="slogan-caption">
                            <h2>
                                <?= $slide->lang->Title ?>
                            </h2>
                        </div>
                    </div>
                    <div class="middle">
                        <div class="details-btn-slider">
                            <a href="<?= $slide->Link ?>">
                                <?= Yii::t("app", "Detalii") ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?> 

    </div>

    <?php } ?>
    
</div>

        
    <div class="uk-clearfix our-services">
        <div class="uk-container-center uk-container">                        
                    
            <?php if (count($services) > 0) { ?>

                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="our-services-title uk-text-center">
                            <h4>
                                <?= Yii::t("app", "Serviciile noastre") ?>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="uk-grid mt70">                                       

                    <?php foreach ($services as $service) { ?>
                    
                    <div class="uk-width-medium-1-3">
                        <div class="our-services-image uk-text-center">
                            <?= Html::img($bundle->baseUrl . '/images/our-services-image.png') ?>
                        </div>
                        <div class="our-services-description uk-text-center mt20">
                            <h3>
                                <?= $service->lang->Title ?>
                            </h3>
                            <p>
                                <?= strip_tags($service->lang->Text) ?>
                            </p>
                        </div>
                    </div>                    
                    
                    <?php } ?>
                </div>
            <?php } ?>
                    
                    
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="order-this mt20">
                            <a class="btn-order" href="#">
                                <?= Yii::t("app", "Comanda acum!") ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-clearfix about-they">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-medium-4-10">
                        <div class="title mt">
                            <h3>
                                <?= Yii::t("app", "Despre noi!") ?>
                            </h3>
                        </div>
                        <div class="paragraph-section mt70">
                            <p>
                                <?= $about->lang->ShortText ?>
                            </p>
                        </div>
                        <div class="our-list">
                            <ul>
                                <li>
                                    <i class="uk-icon-bookmark"></i>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                </li>
                                <li>
                                    <i class="uk-icon-bookmark"></i>
                                    Aliquam tincidunt mauris eu risus.
                                </li>
                                <li>
                                    <i class="uk-icon-bookmark"></i>
                                    Vestibulum auctor dapibus neque.
                                </li>
                                <li>
                                    <i class="uk-icon-bookmark"></i>
                                    Nunc dignissim risus id metus.
                                </li>
                                <li>
                                    <i class="uk-icon-bookmark"></i>
                                    Cras ornare tristique elit.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="uk-width-medium-6-10">
                        <div class="foto mt30">
                            <?= Html::img($bundle->baseUrl . '/images/they-foto.png') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-clearfix they-products">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="title-section">
                            <h3>
                                <?= Yii::t("app", "Produsele noastre") ?>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="uk-grid mt50">
                    
                    <?php foreach ($products as $pr) { ?>
                    <div class="uk-width-medium-1-3">
                        <div class="they-products-box">
                            <div class="img-products">
                                <?= Html::img($pr->MainThumbUrl) ?>
                            </div>
                            <div class="title-products">
                                <h3>
                                    <?= $pr->lang->Title ?>
                                </h3>
                            </div>
                            <div class="price-products">
                                <p>
                                    <?= $pr->lang->ShortText ?>
                                </p>
                            </div>
                            <div class="link-products">
                                <a class="btn-see-products" href="<?= $pr->seoLink ?>">
                                    <?= Yii::t("app", "Vezi produsul") ?>
                                </a>
                            </div>
                        </div>
                    </div>                    
                    <?php } ?>
                                        
                </div>
            </div>
        </div>
