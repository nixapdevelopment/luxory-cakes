<div class="they-products-box" data-pjax="0">
    <div class="img-products">
        <?= yii\helpers\Html::img($model->MainThumbUrl) ?>
    </div>
    <div class="title-products">
        <h3>
            <?= $model->lang->Title ?>
        </h3>
    </div>
    <div class="price-products">
        <p>
            <?= $model->lang->ShortText ?>
        </p>
    </div>
    <div class="link-products">
        <a class="btn-see-products" href="<?= $model->seoLink ?>">
            <?= Yii::t("app", "Vezi produsul") ?>
        </a>
    </div>
</div>

  