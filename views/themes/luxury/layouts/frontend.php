<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\LuxuryFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use app\modules\Settings\Settings;
use yii\helpers\Url;

$bundle = LuxuryFrontAssets::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <!--[if lte IE 8]>
    <style type="text/css">
        body{display:none!important;}
    </style>
    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/">
    <![endif]-->
    <link rel="shortcut icon" href="<?= Settings::getByName("favicon") ? '/uploads/settings/' . Settings::getByName("favicon") : "favico.ico" ?>" type="image/ico">
    <title><?= Html::encode($this->title) ?></title>
    <script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>';</script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="wrapper">
        <div class="uk-clearfix top-header">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-medium-1-2 uk-width-small-1-2">
                        <div class="contact-section">
                            <a href="tel:<?= Settings::getByName("phone") ?>">
                                <i class="uk-icon-phone"></i>
                                 <?= Yii::t("app", "SunatiLa") . " " . Settings::getByName("phone") ?>
                            </a>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2 uk-width-small-1-2">
                        <div class="social-section">
                            <ul>
                                <li>
                                    <a class="facebook" href="<?= Settings::getByName("facebook") ?>">
                                        <?= Html::img($bundle->baseUrl . '/images/facebook.png') ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="instagram" href="<?= Settings::getByName("instagram") ?>">
                                        <?= Html::img($bundle->baseUrl . '/images/instagram.png') ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="youtube" href="<?= Settings::getByName("youtube") ?>">
                                        <?= Html::img($bundle->baseUrl . '/images/youtube.png') ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="twitter" href="<?= Settings::getByName("twitter") ?>">
                                        <?= Html::img($bundle->baseUrl . '/images/twitter.png') ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-clearfix header-nav">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-medium-2-10">
                        <div class="logo uk-animation-slide-bottom">
                            <h1>
                                <a href="<?= Url::home() ?>">
                                    <?= Settings::getByName("logoText") ?>
                                </a>
                            </h1>
                        </div>
                    </div>
                    <div class="uk-width-medium-8-10">
                        <div class="nav-menu">
                        <?= MenuWidget::widget([
                            'menuId' => 3,
                        ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-menu">
                <a href="#mobile_menu" data-uk-offcanvas>
                    <i class="uk-icon-navicon"></i>
                </a>
            </div>
        </div>
        
        <?= $content ?>
        
        <div class="uk-clearfix footer">
            <div class="uk-container-center uk-container">
                <div class="uk-grid">
                    <div class="uk-width-medium-1-3">
                        <div class="footer-box-left">
                            <div class="call">
                                <div class="table">
                                    <div class="middle">
                                        <?= Html::img($bundle->baseUrl . '/images/tel-footer.png') ?>
                                    </div>
                                    <div class="middle">
                                        <a href="tel:<?= Settings::getByName("phone") ?>">
                                            <?= Yii::t("app", "SunatiLa") ?><br />
                                            <?= Settings::getByName("phone") ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="social-contact mt30">
                                    <ul>
                                        <li>
                                             <a href="<?= Settings::getByName("facebook") ?>">
                                                 <?= Html::img($bundle->baseUrl . '/images/footer-facebook.png') ?>
                                             </a>
                                        </li>
                                        <li>
                                            <a  href="<?= Settings::getByName("instagram") ?>">
                                                <?= Html::img($bundle->baseUrl . '/images/instagram-footer.png') ?>
                                            </a>

                                        </li>
                                        <li>
                                            <a class="ninth before after" href="<?= Settings::getByName("youtube") ?>">
                                                <?= Html::img($bundle->baseUrl . '/images/youtube-footer.png') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="ninth before after" href="<?= Settings::getByName("twitter") ?>">
                                                <?= Html::img($bundle->baseUrl . '/images/twitter-footer.png') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="footer-box-middle">
                            <div class="footer-logo">
                                <h1>
                                    <?= Settings::getByName("logoText") ?>
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="footer-box-right">
                            
                            <?= MenuWidget::widget([
                                'menuId' => 4,
                            ]) ?>
                            <?= MenuWidget::widget([
                                'menuId' => 5,
                            ]) ?>
                            
                            <p>
                                Copyright <?= date("Y") ?> - <a href="https://nixap.com">Creat de NIXAP</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- OffCanvas menu -->
    <div id="mobile_menu" class="uk-offcanvas">
        <div class="uk-offcanvas-bar">
            <h1>
                <?= Settings::getByName("logoText") ?>
            </h1>
            <?= MenuWidget::widget([
                'menuId' => 3,
                'linkTemplate' => '<a href="{url}"><i class="uk-icon-birthday-cake"></i>{label}</a>'
                
            ]) ?>            
        </div>
    </div>
     
    
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
