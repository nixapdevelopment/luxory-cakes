<?php

namespace app\models\ArticleLang;

use Yii;

/**
 * This is the model class for table "ArticleLang".
 *
 * @property integer $ID
 * @property integer $ArticleID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 * @property string $SeoTitle
 * @property string $Keywords
 * @property string $Description
 *
 * @property Article $article
 */
class ArticleLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ArticleLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Title', 'Text'], 'required'],
            [['ArticleID'], 'integer'],
            [['Text' , 'ShortText'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'SeoTitle', 'Keywords', 'Description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ArticleID' => 'Article ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Text' => 'Text',
            'ShortText' => 'ShortText',
            'SeoTitle' => 'Seo Title',
            'Keywords' => 'Keywords',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['ID' => 'ArticleID']);
    }
}
