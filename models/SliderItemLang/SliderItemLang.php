<?php

namespace app\models\SliderItemLang;

use Yii;
use app\models\SliderItem\SliderItem;

/**
 * This is the model class for table "SliderItemLang".
 *
 * @property integer $ID
 * @property integer $SliderItemID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property SliderItem $sliderItem
 */
class SliderItemLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SliderItemLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SliderItemID', 'LangID'], 'required'],
            [['SliderItemID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['Text'], 'string', 'max' => 1000],
            [['SliderItemID'], 'exist', 'skipOnError' => true, 'targetClass' => SliderItem::className(), 'targetAttribute' => ['SliderItemID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SliderItemID' => 'Slider Item ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderItem()
    {
        return $this->hasOne(SliderItem::className(), ['ID' => 'SliderItemID']);
    }
}
